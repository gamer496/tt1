$(document).ready(function(){
  $(".friend-link").click(function(event){
    event.preventDefault();
    var id=$(this).attr('id');
    var ele=this;
    function getCookie(c_name) {
    if(document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if(c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if(c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

$(function () {
    $.ajaxSetup({
        headers: {
            "X-CSRFToken": getCookie("csrftoken")
        }
    });
});
    if($(this).hasClass('make-friend')){
      elem=$(this)
      $.ajax({
        url : url,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType:'text',
        data: JSON.stringify({id:id,type:'make-friend'}),
        success:function(result){
          if($.isNumeric(result)){
            $(elem).removeClass('make-friend');
            $(elem).addClass('unfriend')
            $(elem).html('Unfriend');
          }
        }
      });
    }
    if($(this).hasClass('unfriend')){
      elem=$(this)
      $.ajax({
        url : url,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType:'text',
        data: JSON.stringify({id:id,type:'unfriend'}),
        success:function(result){
          if($.isNumeric(result)){
            $(elem).removeClass('unfriend');
            $(elem).addClass('make-friend')
            $(elem).html('Friend');
          }
        }
      });
    }
  });
});
