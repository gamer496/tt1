This is for the technical task related to internship.

To run simply run

        pip install -r requirements.txt

Next in the root directory of your folder create a config.py and declare four variables and assign appropriate value to them:
        
        DB_PASSWORD
        
        HOST_PASSWORD

        HOST_EMAIL_ID

        DB_NAME

Next create a Database in mysql named the value of DB_NAME.

Next run:
        
        python manage.py makemigrations

        python manage.py createsuperuser

        python manage.py migrate

        python manage.py runserver
