from django.conf.urls import url
from fusers import views

urlpatterns=[
    url('^$',views.index,name="fusershome"),
    url(r'^registration',views.registration,name="userregistration"),
    url(r'^activate/(?P<key>.+)$',views.activation,name="useractivation"),
    url(r'^new_activation_link/(?P<user_id>\d+)/$',views.new_activation_link,name="useranotheractivation"),
    url(r'^login',views.fuser_login,name="userlogin"),
    url(r'^logout',views.fuser_logout,name="userlogout"),
    url(r'^viewprofile/(?P<fuser_id>.+)$',views.viewprofile,name="viewprofile"),
    url(r'^makefriend/',views.makefriend,name="makefriend"),
    url(r'^searchfuser',views.searchfuser,name="searchfuser"),
    url(r'^adminstuff',views.adminstuff,name="adminstuff"),
    url(r'^deletefuser/(?P<id>.+)$',views.deletefuser,name="deletefuser"),
]
