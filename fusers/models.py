from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

class Fusers(models.Model):
    user            =models.OneToOneField   (User)
    country         =models.CharField       (default=None,max_length=100)
    fofuser         =models.ManyToManyField (User,related_name='friend_of_user')
    member_since    =models.DateField       (auto_now=True)
    activation_key  =models.CharField       (max_length=40,default="NONE")
    key_expires     =models.DateTimeField   (default=timezone.now)


    def __unicode__(self):
        return self.user.username
