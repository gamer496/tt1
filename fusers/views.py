from django.shortcuts import render,get_object_or_404
from django.contrib.auth.models import User
from fusers.forms import RegistrationForm
from fusers.models import Fusers
from django.http import HttpResponseRedirect,HttpResponse
from django.utils import timezone
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.views import password_reset,password_reset_confirm
from django.core.urlresolvers import reverse
import datetime
import os
import hashlib
import random
import json

def index(request):
    return render(request,"fusers/index.html",{})

def registration(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect("/")
    else:
        if request.method=="POST":
            form=RegistrationForm(request.POST)
            if form.is_valid():
                datas={}
                datas['username']   =form.cleaned_data['username']
                datas['password']   =form.cleaned_data['password']
                datas['first_name'] =form.cleaned_data['first_name']
                datas['last_name']  =form.cleaned_data['last_name']
                datas['email']      =form.cleaned_data['email']
                datas['country']    =form.cleaned_data['country']
                salt=hashlib.sha1(str(random.random())).hexdigest()[:5]
                usernamesalt=datas['username']
                if isinstance(usernamesalt,unicode):
                    usernamesalt=usernamesalt.encode('utf8')
                datas['activation_key']=hashlib.sha1(salt+usernamesalt).hexdigest()
                datas['email_path']="ActivationEmail.txt"
                datas['email_subject']="Activation of your registration"
                form.sendEmail(datas)
                form.save(datas)
                request.session['registered']=True
                return HttpResponseRedirect("/")
            else:
                return render(request,"fusers/registration.html",{'form':form})
        else:
            form=RegistrationForm()
            return render(request,"fusers/registration.html",{'form':form})

def activation(request,key):
    activation_expired=False
    already_active=False
    fuser=get_object_or_404(Fusers,activation_key=key)
    if fuser.user.is_active==False:
        if timezone.now()>fuser.key_expires:
            activation_expired=True
            id_user=fuser.user.id
        else:
            fuser.user.is_active=True
            fuser.user.save()
    else:
        already_active=True
    return HttpResponseRedirect("/")

def new_activation_link(request,user_id):
    form=RegistrationForm()
    datas={}
    user=User.objects.get(id=user_id)
    if user is not None and not user.is_active:
        datas['username']   =user.username
        datas['email']      =user.email
        datas['first_name'] =user.first_name
        datas['last_name']  =user.last_name
        datas['email_path'] ="ResendEmail.txt"
        datas['email_subject']="Email for activation"
        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
        usernamesalt=datas['username']
        if isinstance(usernamesalt,unicode):
            usernamesalt=usernamesalt.encode('utf8')
        datas['activation_key']=hashlib.sha1(salt+usernamesalt).hexdigest()
        fuser=Fusers.objects.get(user=user)
        fuser.activation_key=datas['activation_key']
        datas['country']=fuser.country
        fuser.key_expires=datetime.datetime.strftime(datetime.datetime.now() + datetime.timedelta(days=2), "%Y-%m-%d %H:%M:%S")
        fuser.save()
        form.sendEmail(datas)
        request.session['new_link']=True
    return HttpResponseRedirect("/")

def fuser_login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect("/")
    elif request.method=="POST":
        username=request.POST["username"]
        password=request.POST["password"]
        user=authenticate(username=username,password=password)
        if user:
            login(request,user)
            return HttpResponseRedirect("/")
        else:
            return render(request,"fusers/login.html",{})
    else:
        return render(request,"fusers/login.html",{})

def fuser_logout(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect("/")
    else:
        logout(request)
        return HttpResponseRedirect(reverse("userlogin"))

def viewprofile(request,fuser_id):
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('userlogin'))
    fuser=get_object_or_404(Fusers,pk=fuser_id)
    own_id=3
    if request.user.is_superuser:
        return render(request,"fusers/viewprofile.html",{'fuser':fuser,'own_id':4})
    vfuser=get_object_or_404(Fusers,user=request.user)
    k=vfuser.fofuser.filter(username=fuser.user.username).exists()
    if fuser.user==request.user:
        own_id=1
    elif k==True:
        own_id=2
    return render(request,"fusers/viewprofile.html",{'fuser':fuser,'own_id':own_id})

def makefriend(request):
    json_data=json.loads(request.body)
    id=int(json_data["id"])
    fuser=get_object_or_404(Fusers,pk=id)
    cuser=get_object_or_404(Fusers,user=request.user)
    if json_data["type"]=="unfriend":
        cuser.fofuser.remove(fuser.user)
    else:
        cuser.fofuser.add(fuser.user)
    return HttpResponse("1")


def searchfuser(request):
    allusers=User.objects.all()
    lusers=[]
    for user in allusers:
        lusers.append(user.username)
    if not request.user.is_authenticated():
        return HttpResponseRedirect(reverse('userlogin'))
    if request.method=="POST":
        name=request.POST["searchsomefuser"]
        users=[]
        vfuser=get_object_or_404(Fusers,user=request.user)
        for fuser in Fusers.objects.all():
            if fuser.user.username==name or fuser.country==name or fuser.user.first_name==name or fuser.user.last_name==name:
                k=vfuser.fofuser.filter(username=fuser.user.username).exists()
                if k==True:
                    k=2
                elif fuser.user==request.user:
                    k=1
                else:
                    k=3
                users.append([fuser,k])
        return render(request,"fusers/searchfuser.html",{'users':users,'lusers':lusers})
    else:
        return render(request,"fusers/searchfuser.html",{'lusers':lusers})

def adminstuff(request):
    if not request.user.is_superuser:
        return HttpResponseRedirect(reverse("fusershome"))
    else:
        if request.method=="POST":
            emailid=request.POST["emailid"]
            user=get_object_or_404(User,email=emailid)
            fuser=get_object_or_404(Fusers,user=user)
            friends=[]
            for friend in fuser.fofuser.all():
                ffuser=get_object_or_404(Fusers,user=friend)
                friends.append(ffuser)
            return render(request,"fusers/adminstuff.html",{'friends':friends,'fuser':fuser})
        else:
            return render(request,"fusers/adminstuff.html",{})

def deletefuser(request,id):
    if not request.user.is_superuser:
        return HttpResponseRedirect(reverse('mainindex'))
    else:
        print id
        fuser=get_object_or_404(Fusers,pk=id)
        fuser.delete()
        return render(request,"fusers/deletesuccess.html",{})
